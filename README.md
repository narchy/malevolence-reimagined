# Malevolence Maps Reimagined!

This FoundryVTT module is compilation of maps for the Pathfinder Second Edition MALEVOLENCE adventure.

All maps are designed for [FoundryVTT](https://foundryvtt.com/) - with walls, doors and windows set up. If you find a mistake, let me know. 

The module does not contain any of the material or content from the book, and you will need to purchase your own copy to play it. You should support Paizo and their awesome product by [buying the darn books](https://paizo.com/products/btq024xn).

Find @narchy on https://discord.gg/foundryvtt for updates!

Everything is free, but if you want to make a donation to my RPG book buying addiction, then feel free! https://www.buymeacoffee.com/narchym or https://www.ko-fi.com/narchymaps

# Credit

PAIZO - Some of the maps uses trademarks and/or copyrights owned by Paizo Inc., used under [Paizo's Community Use Policy](https://www.paizo.com/communityuse). We are expressly prohibited from charging you to use or access this content. The maps are not published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, [visit paizo.com](http://www.paizo.com).

FORGOTTEN ADVENTURES - Assets used to create this map are from [Forgotten Adventures](https://www.forgotten-adventures.net/info/). These guys rock.

CC0 TEXTURES - Contains assets from [CC0Textures.com](https://www.CC0Textures.com), licensed under CC0 1.0 Universal.

NARCHY - Hey, it's me! Some of the weirder assets are my own creations.

Thank you to [Ustin](https://gitlab.com/Ustin/) for pointing me in the right direction on how to setup this module. Cheers to Kalnix for helping with the walls.

# More Maps

I make maps for most of the games I run, and they are [freely available to download here on Google Drive](https://www.tinyurl.com/narchymaps). You can go ahead and use them freely without installing this module, and you will find maps in there long before I get around to adding them to a module! All maps are stamped with the grid size, so you can set them up easily in your virtual tabletop environment of choice. 


[![Preview Image](preview/preview.jpg)](https://gitlab.com/narchy-maps/malevolence-reimagined/-/raw/master/preview/preview.jpg)
